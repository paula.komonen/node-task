# Node Task

This project contains node
hello-world

## Installation steps

1. Copy the repository `git clone`
2. Install the dependencies
3. Start the app

## Terminal commands

Code block:

```sh
git clone https://gitlab.com/karrimiettinen/node-task
cd ./node-task
npm i
```